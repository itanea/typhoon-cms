## About Typhoon CMS

* First of all, this project is on early alpha stage. So, please don't expect nothing from it now ! *

Typhoon wishes to be the simplest way to manage your website !

## Typhoon Sponsors

You could support this work if you want by tipping me on [Paypal](https://paypal.me/itanea).

## Contributing

Thank you for considering contributing to the Typhoon CMS! The contribution guide will be write as soon as possible.


## Security Vulnerabilities

If you discover a security vulnerability within Typhoon, please send me an e-mail to [fred@itanea.fr](mailto:fred@itanea.fr). All security vulnerabilities will be promptly addressed.

## License

The Typhoon CMS is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
